import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';


const style = {
  margin: 12,
};

class Main extends Component {
    
  render() {
    return (
      <div >
      <p>{this.props.title}</p>
        <RaisedButton label="Giriş"
                                primary={true}
                                 onTouchTap={()=> console.log("tıklandı")}
                                />
      </div>
    );
  }
}

export default Main;
