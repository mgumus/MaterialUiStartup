import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import Dialog from 'material-ui/Dialog';
import {grey500, white,} from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';

  const styles = {
    loginContainer: {
      maxWidth:400,
      height: 'auto',
      position: 'absolute',
      top: '20%',
      left: 0,
      right: 0,
      margin: 'auto'
    },
    paper: {
      padding: 20,
      overflow: 'auto'
    },
    buttonsDiv: {
      textAlign: 'center',
      padding: 10
    },
    flatButton: {
      color: grey500
    },
    checkRemember: {
      style: {
        float: 'left',
        maxWidth: 180,
        paddingTop: 5
      },
      labelStyle: {
        color: grey500
      },
      iconStyle: {
        color: grey500,
        borderColor: grey500,
        fill: grey500
      }
    },
    loginBtn: {
      float: 'right'
    },
    btn: {
      background: '#4f81e9',
      color: white,
      padding: 7,
      borderRadius: 2,
      margin: 2,
      fontSize: 13
    },
    btnFacebook: {
      background: '#4f81e9'
    },
    btnGoogle: {
      background: '#e14441'
    },
    btnSpan: {
      marginLeft: 5
    },
  }

class Login extends Component {

  render() {
    return (
       <div style={{backgroundImage:"url("+require('../images/istanbul2.jpg')+")", }}>
        <div style={styles.loginContainer} className="col-xs-12 col-sm-6">
          <div> {this.props.title} </div>
          <Paper style={styles.paper}  >
            <form>
              <TextField
                hintText="Username"
                floatingLabelText="Username"
                fullWidth={true}
                onChange={(i)=>{this.setState({username:i.target.value})}}
              />
              <TextField
                hintText="Password"
                floatingLabelText="Password"
                fullWidth={true}
                type="password"
                onKeyPress={this.handleKeyPress}
                onChange={(i)=>{this.setState({password:i.target.value})}}
              />

              <div>
                <Checkbox
                  label="Remember me"
                  style={styles.checkRemember.style}
                  labelStyle={styles.checkRemember.labelStyle}
                  iconStyle={styles.checkRemember.iconStyle}
                />

                
                  <RaisedButton label="Giriş"
                                primary={true}
                                style={styles.loginBtn}
                                 onTouchTap={this._login}
                                />
               
              </div>
            </form>
          </Paper>
        </div>
      </div>
    );
  }
}

/* 


        <Dialog
          title={this.state.dialog.title ? this.state.dialog.title : "Hata"}
          modal={false}
          open={this.state.dialog.open}
          onRequestClose={()=> {this.setState({dialog:{open:false}})}}
        >
          {this.state.dialog.description ? this.state.dialog.description : "Beklenmeyen bir hata oluştu"}
        </Dialog>

        */

export default Login;
